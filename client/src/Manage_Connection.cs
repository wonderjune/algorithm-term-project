﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Alg_CLIENT
{    
   
    public class Manage_Connection
    {

        private NetworkStream NetStream;
        private StreamReader READ_Stream;
        private StreamWriter WRITE_Stream;


        private IPEndPoint  SERVER_Address;
        private Socket      SERVER_Socket;

        
        public Manage_Connection()
        {
        }


        public void Open_Connection( string SERVER_IP_Address, int SERVER_PortNumber)
        {

            this.SERVER_Address         = new IPEndPoint    (   IPAddress.Parse(SERVER_IP_Address), SERVER_PortNumber);
            this.SERVER_Socket          = new Socket        (   AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


            this.SERVER_Socket.Connect(this.SERVER_Address);


            this.NetStream      = new NetworkStream(this.SERVER_Socket);
            this.WRITE_Stream   = new StreamWriter(this.NetStream);
            this.READ_Stream    = new StreamReader(this.NetStream);


            Console.WriteLine("connected");
        }

        public void Send_and_Receive(int Freq, Manage_Transaction oMT)
        {
            string Msg = "TICK";
	    oMT.WRITE_Stream = this.WRITE_Stream;


            while( Msg.Equals( "END" ) == false )
            {
                if (this.SERVER_Socket.Connected)
                {
                    // SEND         'TICK' to SERVER
                    WRITE_Stream.WriteLine("TICK");
                    WRITE_Stream.Flush();       

                    // RECEIVE      ONE TICK from SERVER
                    Msg = READ_Stream.ReadLine();

                    // PRINT        ONE TICK to console

		    if (Msg.Equals("END")) continue;

		    string[] result = Msg.Split('/');
		    Tick tick = new Tick(result);
		    History.commit(tick);
		    History.Update();
		    oMT.Update(tick);

                    // if Some Condition is 

                    // Stop Thread for ' Freq ' ms
                    Thread.Sleep(Freq);

                }

                else
                    Console.WriteLine("fail");
            }

	    History.Print_Statistics();
        }

        public void Close_Connection()
        {
            this.SERVER_Socket.Close();
        }

        public void Wait()
        {
            READ_Stream.ReadLine();
        }

        public  IPEndPoint  Server_Address
        {
            get { return this.SERVER_Address; }
        }

        public  Socket      Server_Socket
        {
            get { return this.SERVER_Socket; }
        }

    }
    

}
