using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Alg_CLIENT
{

public class Manage_Transaction
{
	public StreamWriter WRITE_Stream;

	public Manage_Transaction() {
	}

	public void Update(Tick tick) {
		Transaction tx = new Transaction(tick);

		if (History.TxHistory.ContainsKey(tick.CODE))
		{
			List<Transaction> list = History.TxHistory[tick.CODE];
			if (list[list.Count - 1].TIME - tick.TIME > -1000)
				return;
		}

		if (History.MeanPriceMonthly[tick.CODE] < History.PrevMeanPriceMonthly[tick.CODE] * 0.8 &&
			History.MeanPriceWeekly[tick.CODE] < History.PrevMeanPriceWeekly[tick.CODE] * 0.8)
		{
			tx.TXTYPE = "S";
			tx.AMOUNT = (int) (History.AmountOfStock[tick.CODE] * 0.9);
		}
		else if (History.MeanPriceWeekly[tick.CODE] < History.PrevMeanPriceWeekly[tick.CODE] &&
				History.MeanPriceDaily[tick.CODE] > (History.PrevMeanPriceDaily[tick.CODE] +
					History.PrevMeanPriceWeekly[tick.CODE] - History.PrevMeanPriceWeekly[tick.CODE]))
		{
			tx.TXTYPE = "B";
			tx.AMOUNT = (int) (tick.TICK_VOLUME * 0.9);
		}
		else if (tick.PRICE > History.PrevMeanPriceDaily[tick.CODE] * 1.1)
		{
			tx.TXTYPE = "B";
			tx.AMOUNT = (int) (tick.TICK_VOLUME * 0.5);
		}
		else if (tick.PRICE < History.PrevMeanPriceDaily[tick.CODE] * 0.7)
		{
			tx.TXTYPE = "S";
			tx.AMOUNT = (int) (History.AmountOfStock[tick.CODE] * 0.5);
		}
		else if (tick.PRICE < History.MeanPriceHourly[tick.CODE] &&
				History.MeanPriceHourly[tick.CODE] < History.PrevMeanPriceHourly[tick.CODE] * 0.9)
		{
			tx.TXTYPE = "B";
			tx.AMOUNT = (int) (tick.TICK_VOLUME * 0.1);
		}
		else
		{
			return;
		}

		if (tx.TXTYPE == "B" && History.AssetInMoney - tx.AMOUNT * tx.PRICE < 0)
		{
			return;
			if (History.AssetInMoney * 9 < tx.AMOUNT * tx.PRICE)
				tx.AMOUNT = (int) (History.AssetInMoney * 0.4);
			else if (History.AssetInMoney * 3 < tx.AMOUNT * tx.PRICE)
				tx.AMOUNT = (int) (History.AssetInMoney * 0.2);
			else
				return;
		}

		Apply_Transaction(tx);
	}

	private void Apply_Transaction(Transaction tx)
	{
		tx.DumpTo(WRITE_Stream);
		History.commit(tx);
	}
}

}
