﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace Alg_CLIENT
{
    public class Manage_FileInOut
    {
        string[] Basic_Information;
        
        private string      IPAddress_SERVER; //= "127.0.0.1";
        private int         PortNum_SERVER;
        private int         Number_Of_Company;
        private int         Freq;


        public Manage_FileInOut                                         (   string File_Path)
        {
            this.Basic_Information = Read_BasicInformation(File_Path);

            // IP Address of SERVER
            this.IPAddress_SERVER   = this.Basic_Information[0];

            // Port Number of SERVER
            this.PortNum_SERVER     = String_to_Int( this.Basic_Information[1] );

            // Repetition Times per sec
            if( String_to_Int(this.Basic_Information[3])  > 0 )
                this.Freq               = (int) ( (float)1000 / String_to_Int(this.Basic_Information[2]) );

            else
            {
                this.Freq = 1;
                Console.WriteLine(" 'Repetition Times per sec' in BasicInformation.txt file is inappropriate ");
            }

            // Number of Company that you should monitor
            this.Number_Of_Company = String_to_Int(this.Basic_Information[3]);
        }


        private string []   Read_BasicInformation                       (   string File_Path )
        {
            string[] BasicInformation = new string[4];
            int cnt = 0;

            FileStream file     = new FileStream(File_Path, FileMode.Open, FileAccess.Read);
            StreamReader sr     = new StreamReader(file);


            while (sr.EndOfStream == false)
            {
                BasicInformation[cnt] = sr.ReadLine();
                cnt++;
            }              
                

            sr.Close();
            file.Close();

            return BasicInformation;
        }

        private int         String_to_Int                               (   string strings)
        {
            long returnvalue = 0;
            long CurValue = 0;
            int Length = 0;


            // 길이를 구한다
            Length = strings.Length;

            for (int i = 0; i <= Length - 1; i++)
            {
                CurValue = strings[i] - '0';

                for (int j = 0; j < (Length - 1 - i); j++)
                {
                    CurValue = CurValue * 10;
                }
                returnvalue = returnvalue + CurValue;
            }


            return (int)returnvalue;
        }




        public string IPaddressSERVER
        {
            get { return this.IPAddress_SERVER; }
        }

        public int PortNumberSERVER
        {
            get { return this.PortNum_SERVER; }
        }

        public int NumberOfCompany
        {
            get { return this.Number_Of_Company; }
        }

        public int Frequency
        {
            get { return this.Freq; } 
        }

    }
}
