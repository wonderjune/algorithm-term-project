﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alg_CLIENT
{
        public struct Tick
        {
            public Tick(string[] result) {
		    this.CODE = result[0];
		    this.DATE = Convert.ToInt32(result[1]);
		    this.TIME = Convert.ToInt32(result[2]);
		    this.PRICE = Convert.ToInt32(result[3]);
		    this.TICK_VOLUME = Convert.ToInt32(result[4]);
		    this.INC_OR_DEC = Convert.ToInt32(result[5]);
	    }
            public string CODE;
            public int DATE;
            public int TIME;
            public int PRICE;

            public int TICK_VOLUME;
            public int INC_OR_DEC;              //  Cons.VOL.INC        or       Cons.VOL.DEC

	    public void DumpTo(TextWriter writer) {
		    writer.WriteLine("{0}/{1}/{2}/{3}/{4}/{5}", CODE, DATE, TIME, PRICE, TICK_VOLUME, INC_OR_DEC);
	    }
        }

        public struct Transaction
        {
	    public Transaction(Tick tick) {
		    this.CODE = tick.CODE;
		    this.TXTYPE = "B";
		    this.DATE = tick.DATE;
		    this.TIME = tick.TIME;
		    this.PRICE = tick.PRICE;
		    this.AMOUNT = 0;
	    }
            public string   CODE;
            public string   TXTYPE;

            public int DATE;
            public int TIME;
            public int      PRICE;
            public int      AMOUNT;

	    public void DumpTo(TextWriter writer) {
		    writer.WriteLine("{0}/{1}/{2}/{3}", CODE, TXTYPE, PRICE, AMOUNT);
	    }
        }

}

