﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;

namespace Alg_CLIENT
{
    public class Program
    {
        public static void Main(string[] args)
        {
            #region [0] Read Basic Info

                // Read BasicInformation.txt
                Manage_FileInOut oMFIO = new Manage_FileInOut("config");
            
            #endregion
		
		Manage_Transaction oMT = new Manage_Transaction();

            #region [1] Open Connection

                // Open Connection to Server
                Manage_Connection oMC = new Manage_Connection();
                oMC.Open_Connection     (   oMFIO.IPaddressSERVER, oMFIO.PortNumberSERVER);

                oMC.Send_and_Receive    (   oMFIO.Frequency, oMT);

            #endregion

            #region [2] Wait

                oMC.Wait();

            #endregion


                oMC.Close_Connection();

        }
    }
}
