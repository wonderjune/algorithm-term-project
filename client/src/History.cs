using System;
using System.Collections;
using System.Collections.Generic;

namespace Alg_CLIENT
{

public static class History
{
	public static Dictionary<string, List<Tick> >  TickHistory;
	public static Dictionary<string, List<Transaction> > TxHistory;

	public static int TotalCost;

	public static int AssetInMoney;
	public const int FIRST_MONEY = 10000000;
	public static Dictionary<string, int> AmountOfStock;
	public static Dictionary<string, int> LastUpdateTxCount;

	public static Dictionary<string, int> MeanPriceHourly;
	public static Dictionary<string, int> MeanPriceDaily;
	public static Dictionary<string, int> MeanPriceWeekly;
	public static Dictionary<string, int> MeanPriceMonthly;

	public static Dictionary<string, int> PrevMeanPriceHourly;
	public static Dictionary<string, int> PrevMeanPriceDaily;
	public static Dictionary<string, int> PrevMeanPriceWeekly;
	public static Dictionary<string, int> PrevMeanPriceMonthly;

	static History()
	{
		TickHistory = new Dictionary<string, List<Tick> >();
		TxHistory = new Dictionary<string, List<Transaction> >();
	
		TotalCost = 0;

		AssetInMoney = FIRST_MONEY;
		AmountOfStock = new Dictionary<string, int>();
		LastUpdateTxCount = new Dictionary<string, int>();

		MeanPriceHourly = new Dictionary<string, int>();
		MeanPriceDaily = new Dictionary<string, int>();
		MeanPriceWeekly = new Dictionary<string, int>();
		MeanPriceMonthly = new Dictionary<string, int>();

		PrevMeanPriceHourly = new Dictionary<string, int>();
		PrevMeanPriceDaily = new Dictionary<string, int>();
		PrevMeanPriceWeekly = new Dictionary<string, int>();
		PrevMeanPriceMonthly = new Dictionary<string, int>();
	}

	public static void commit(Tick tick)
	{
		if (!TickHistory.ContainsKey(tick.CODE))
			TickHistory.Add(tick.CODE, new List<Tick>());
		TickHistory[tick.CODE].Add(tick);
//		tick.DumpTo(Console.Out);
	}

	public static void commit(Transaction tx)
	{
		if (!TxHistory.ContainsKey(tx.CODE))
			TxHistory.Add(tx.CODE, new List<Transaction>());
		TxHistory[tx.CODE].Add(tx);
//		tx.DumpTo(Console.Out);
	}

	public static void Update()
	{
#region [1] AmountOfStock and LastUpdateTxCount
		foreach (KeyValuePair<string, List<Transaction> > txList in TxHistory)
		{
			if (!AmountOfStock.ContainsKey(txList.Key))
				AmountOfStock.Add(txList.Key, 0);
			if (!LastUpdateTxCount.ContainsKey(txList.Key))
				LastUpdateTxCount.Add(txList.Key, 0);

			for (int i = 1; i <= txList.Value.Count - LastUpdateTxCount[txList.Key]; i++)
			{
				Transaction tx = txList.Value[txList.Value.Count - i];
				if (tx.TXTYPE.Equals("B"))
				{
					AssetInMoney -= (int) Math.Ceiling(tx.PRICE * tx.AMOUNT * 1.0033);
					TotalCost += (int) Math.Floor(tx.PRICE * tx.AMOUNT * 0.0033);
					AmountOfStock[tx.CODE] += tx.AMOUNT;
				}
				else if (tx.TXTYPE.Equals("S"))
				{
					AssetInMoney += (int) Math.Floor(tx.PRICE * tx.AMOUNT * 0.9967);
					TotalCost += (int) Math.Ceiling(tx.PRICE * tx.AMOUNT * 0.0033);
					AmountOfStock[tx.CODE] -= tx.AMOUNT;
				}
			}
			LastUpdateTxCount[txList.Key] = txList.Value.Count;
		}
#endregion //[1]

#region [2] MeanPriceHourly, ...
		foreach (KeyValuePair<string, List<Tick> > tickListPair in TickHistory)
		{
			string code = tickListPair.Key;
			List<Tick> tickList = tickListPair.Value;
			int lastDate = tickList[tickList.Count - 1].DATE;
			int lastTime = tickList[tickList.Count - 1].TIME;
			int count0, sumOfPrice0, mean0;
			int count1, sumOfPrice1, mean1;

			count0 = 0;
			count1 = 0;
			sumOfPrice0 = 0;
			sumOfPrice1 = 0;
			for (int i = tickList.Count; i > 0; i--)
			{
				Tick tick = tickList[i - 1];
				if (tick.DATE != lastDate || tick.TIME - lastTime < -20000)
				{
					break;
				}
				else if (tick.TIME - lastTime < -10000)
				{
					sumOfPrice1 += tick.PRICE;
					count1++;
				}
				else
				{
					sumOfPrice0 += tick.PRICE;
					count0++;
				}
			}
			mean0 = (int) Math.Round(1.0 * sumOfPrice0 / count0);
			mean1 = (int) Math.Round(1.0 * sumOfPrice1 / count1);
			if (count1 == 0)
				mean1 = mean0;
			MeanPriceHourly[code] = mean0;
			PrevMeanPriceHourly[code] = mean1;

			count0 = 0;
			count1 = 0;
			sumOfPrice0 = 0;
			sumOfPrice1 = 0;
			for (int i = tickList.Count; i > 0; i--)
			{
				Tick tick = tickList[i - 1];
				if (tick.DATE - lastDate < -1)
				{
					break;
				}
				else if (tick.DATE - lastDate == -1)
				{
					sumOfPrice1 += tick.PRICE;
					count1++;
				}
				else
				{
					sumOfPrice0 += tick.PRICE;
					count0++;
				}
			}
			mean0 = (int) Math.Round(1.0 * sumOfPrice0 / count0);
			mean1 = (int) Math.Round(1.0 * sumOfPrice1 / count1);
			if (count1 == 0)
				mean1 = mean0;
			MeanPriceDaily[code] = mean0;
			PrevMeanPriceDaily[code] = mean1;

			count0 = 0;
			count1 = 0;
			sumOfPrice0 = 0;
			sumOfPrice1 = 0;
			for (int i = tickList.Count; i > 0; i--)
			{
				Tick tick = tickList[i - 1];
				if (tick.DATE - lastDate < -14)
				{
					break;
				}
				else if (tick.DATE - lastDate < -7)
				{
					sumOfPrice1 += tick.PRICE;
					count1++;
				}
				else
				{
					sumOfPrice0 += tick.PRICE;
					count0++;
				}
			}
			mean0 = (int) Math.Round(1.0 * sumOfPrice0 / count0);
			mean1 = (int) Math.Round(1.0 * sumOfPrice1 / count1);
			if (count1 == 0)
				mean1 = mean0;
			MeanPriceWeekly[code] = mean0;
			PrevMeanPriceWeekly[code] = mean1;


			count0 = 0;
			count1 = 0;
			sumOfPrice0 = 0;
			sumOfPrice1 = 0;
			for (int i = tickList.Count; i > 0; i--)
			{
				Tick tick = tickList[i - 1];
				if (tick.DATE - lastDate < -200)
				{
					break;
				}
				else if (tick.DATE - lastDate < -100)
				{
					sumOfPrice1 += tick.PRICE;
					count1++;
				}
				else
				{
					sumOfPrice0 += tick.PRICE;
					count0++;
				}
			}
			mean0 = (int) Math.Round(1.0 * sumOfPrice0 / count0);
			mean1 = (int) Math.Round(1.0 * sumOfPrice1 / count1);
			if (count1 == 0)
				mean1 = mean0;
			MeanPriceMonthly[code] = mean0;
			PrevMeanPriceMonthly[code] = mean1;
		}
#endregion //[2]
	}

	public static void Print_Statistics()
	{
		int AssetInStock = 0;
		
		Update();

		int txCountAll = 0;
		foreach (KeyValuePair<string, List<Transaction> > txListPair in TxHistory)
			txCountAll += txListPair.Value.Count;
		Console.WriteLine("{0} Transactions made\n", txCountAll);
		
		foreach (KeyValuePair<string, int> stocks in AmountOfStock)
		{
			List<Tick> tickList = TickHistory[stocks.Key];
			int stockLastPrice = tickList[tickList.Count - 1].PRICE;
			AssetInStock += stocks.Value * stockLastPrice;
		}

		Console.WriteLine("{0} --> {1} ({2} + {3}) = {4}%",
			FIRST_MONEY,
			AssetInMoney + AssetInStock,
			AssetInMoney,
			AssetInStock,
			(100.0 * (AssetInMoney + AssetInStock) / FIRST_MONEY));

	}
}

}
