﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.IO;

using System.Net;
using System.Net.Sockets;



namespace Alg_SERVER
{
    public class Manage_Transmission_Reception
    {

        private TRANSACTION[]   TRANSACTION_LIST;
        private int             cntTRL;


        private NetworkStream   NetStream;
        private StreamReader    READ_Stream;
        private StreamWriter    WRITE_Stream;

        private string          Received_Msg;
        private string[]        Received_Transaction_Signal;
        private bool            Received_END ;




        public          Manage_Transmission_Reception       (   Socket CLIENT_Socket, int MAXLENGTH_OF_TRLIST)
        {
            this.TRANSACTION_LIST   = new TRANSACTION[MAXLENGTH_OF_TRLIST];
            this.cntTRL             = 0;

            this.Received_Msg   = null;
            this.Received_END   = false;

            this.NetStream      = new NetworkStream (CLIENT_Socket);
            this.READ_Stream    = new StreamReader  (this.NetStream);
            this.WRITE_Stream   = new StreamWriter  (this.NetStream);
        }

        public void     ReceiveMsg                          (   Socket CLIENT_Socket, Manage_TickData oMTD,  Manage_FileInOut oMFIO, int Number_Of_Company)
        {
            int IDXTL_toBUY = 0;
            int IDXTL_toSELL = 0;


            while ( (CLIENT_Socket.Connected) && (this.Received_END == false) )
            {
                // Read Line
                this.Received_Msg = READ_Stream.ReadLine();                
           
                // Received TICK-Signal
                if (Received_Msg.Equals("TICK"))
                {
                    #region [1] END OF DATA

                        if (oMTD.ENDOF_TICK_DATA == true)
                        {
                            Console.WriteLine("END");

                            // Send "END"
                            WRITE_Stream.WriteLine("END");
                            WRITE_Stream.Flush();
                            this.Received_END = true;
                        }
                    
                    #endregion

                    #region [2] END OF DAY

                        else if (oMTD.ENDOF_ONE_DAY == true)
                        {
                            //Console.WriteLine("ONE Tick: {0} ", oMTD.CURTICK);
                            // 마지막이 아니고 하루의 끝일 때

                            WRITE_Stream.WriteLine(oMTD.CURTICK);
                            WRITE_Stream.Flush();                            


                            oMTD.Initialize();
                            oMTD.getTickDATA_Make_TickList(oMFIO, Number_Of_Company);
                            oMTD.Index_OneTick_afterSEND();
                        }

                    #endregion

                    #region [3] else

                        else
                        {
                            //Console.WriteLine("ONE Tick: {0} ", oMTD.CURTICK);

                            // Send Cur Tick
                            WRITE_Stream.WriteLine(oMTD.CURTICK);
                            WRITE_Stream.Flush();
                            oMTD.Index_OneTick_afterSEND();

                        }

                    #endregion                   
                }

                // Received Transaction-Signal
                else
                {                    
                    this.Received_Transaction_Signal = Split_Rcvd_Msg(this.Received_Msg);

                    // Received BUY Signal
                    if (this.Received_Transaction_Signal[1].Equals("B"))
                    {
                        // FIND LATEST TICK
                        IDXTL_toBUY = FIND_LATEST_IDXTL(this.Received_Transaction_Signal[0], oMTD);                        


                        // STORE
                        if (this.cntTRL < this.TRANSACTION_LIST.Length)
                        {
                            /*
                            Console.WriteLine("B-1 Tick: {0} {1} {2} ", oMTD.SortedTickList[IDXTL_toBUY-1].SH_CODE, oMTD.SortedTickList[IDXTL_toBUY-1].TIME, oMTD.SortedTickList[IDXTL_toBUY-1].PRICE);
                            Console.WriteLine("B Tick:   {0} {1} {2} ",   oMTD.SortedTickList[IDXTL_toBUY].SH_CODE, oMTD.SortedTickList[IDXTL_toBUY].TIME, oMTD.SortedTickList[IDXTL_toBUY].PRICE);
                            Console.WriteLine("B+1 Tick: {0} {1} {2} ", oMTD.SortedTickList[IDXTL_toBUY+1].SH_CODE, oMTD.SortedTickList[IDXTL_toBUY+1].TIME, oMTD.SortedTickList[IDXTL_toBUY+1].PRICE);
                            Console.WriteLine();
                            /**/

                            this.TRANSACTION_LIST[this.cntTRL] = STORE_ONE_ACTION(oMTD, IDXTL_toBUY, this.Received_Transaction_Signal);
                            this.cntTRL++;
                        }
                        else
                            Console.WriteLine("Too many transactions. Increase the value of variable 'MAXLENGTH_OF_TRLIST' in Program.cs and try again. ");
                    }

                     // Received SELL Signal
                    else if (this.Received_Transaction_Signal[1].Equals("S"))
                    {
                        IDXTL_toSELL = FIND_LATEST_IDXTL(this.Received_Transaction_Signal[0], oMTD);
                        
                        // STORE
                        if (this.cntTRL < this.TRANSACTION_LIST.Length)
                        {
                            /*
                            Console.WriteLine("S-1 Tick: {0} {1} {2} ", oMTD.SortedTickList[IDXTL_toSELL - 1].SH_CODE, oMTD.SortedTickList[IDXTL_toSELL - 1].TIME, oMTD.SortedTickList[IDXTL_toSELL - 1].PRICE);
                            Console.WriteLine("S Tick:   {0} {1} {2} ", oMTD.SortedTickList[IDXTL_toSELL].SH_CODE, oMTD.SortedTickList[IDXTL_toSELL].TIME, oMTD.SortedTickList[IDXTL_toSELL].PRICE);
                            Console.WriteLine("S+1 Tick: {0} {1} {2} ", oMTD.SortedTickList[IDXTL_toSELL + 1].SH_CODE, oMTD.SortedTickList[IDXTL_toSELL + 1].TIME, oMTD.SortedTickList[IDXTL_toSELL + 1].PRICE);
                            Console.WriteLine();
                            /**/
                            this.TRANSACTION_LIST[this.cntTRL] = STORE_ONE_ACTION(oMTD, IDXTL_toSELL, this.Received_Transaction_Signal);
                            this.cntTRL++;
                        }

                        else
                            Console.WriteLine("Too many transactions. Increase the value of variable 'MAXLENGTH_OF_TRLIST' in Program.cs and try again. ");
                    }
                }
            }


            if (CLIENT_Socket.Connected == false)
                Console.WriteLine("Disconnected");
        }

        public void     Wait                                ()
        {
            READ_Stream.ReadLine();  
        }


        private string[]        Split_Rcvd_Msg              (   string Rcvd_Msg )
        {
            char[] Spliter = new char[1];
            string[] eachINFO = new string[3];                
            Spliter[0] = '/';
            eachINFO = Rcvd_Msg.Split(Spliter, StringSplitOptions.RemoveEmptyEntries);
            return eachINFO;
        }

        private int             FIND_LATEST_IDXTL           (   string CODE, Manage_TickData oMTD)
        {
            if (oMTD.curIDXTL <= 1)
                return 0 ;

            for (int i = oMTD.curIDXTL - 2; i >= 0; i--)
            {
                if (oMTD.SortedTickList[i].SH_CODE.Equals(CODE))
                    return i;
            }

            Console.WriteLine("There is no such company : {0}    Check your company code or signal form ", CODE);
            return 0;
        }

        private TRANSACTION     STORE_ONE_ACTION            (   Manage_TickData oMTD, int idxTL, string [] Rcvd_TR_SIG )
        {
            // Rcvd_TR_SIG[0] ; CODE
            // Rcvd_TR_SIG[1] ; B or S
            // Rcvd_TR_SIG[2] ; Amount

            TRANSACTION ONE_ACTION = new TRANSACTION();


            ONE_ACTION.CODE                 = oMTD.SortedTickList[idxTL].SH_CODE;
            ONE_ACTION.TRANSACTION_FORM     = Rcvd_TR_SIG[1]; 


            ONE_ACTION.DATE                 = oMTD.SortedTickList[idxTL].DATE ;
            ONE_ACTION.TIME                 = oMTD.SortedTickList[idxTL].TIME ;
            ONE_ACTION.PRICE                = oMTD.SortedTickList[idxTL].PRICE ;

            ONE_ACTION.AMOUNT               = String_to_Int( Rcvd_TR_SIG[2]) ;


            return ONE_ACTION; 
        }

        private static int      String_to_Int               (   string strings)
        {
            long returnvalue = 0;
            long CurValue = 0;
            int Length = 0;


            // 길이를 구한다
            Length = strings.Length;

            for (int i = 0; i <= Length - 1; i++)
            {
                CurValue = strings[i] - '0';

                for (int j = 0; j < (Length - 1 - i); j++)
                {
                    CurValue = CurValue * 10;
                }
                returnvalue = returnvalue + CurValue;
            }


            return (int)returnvalue;
        }




        public TRANSACTION []TRANSACTIONLIST
        {
            get { return this.TRANSACTION_LIST; }
        }

        public int lastTRL
        {
            get { return this.cntTRL; }
        }


    }
}
