﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alg_SERVER
{
        public struct Tick
        {
            public string SH_CODE;
            public int DATE;
            public int TIME;
            public int PRICE;

            public int TICK_VOLUME;
            public int INC_OR_DEC;              //  Cons.VOL.INC        or       Cons.VOL.DEC
        }

        public struct IIDX
        {
            public int          DATE;
            public string   []  PATH;
            public int          count;


            public IIDX(int Length)
            {
                this.DATE           = 0;
                this.PATH           = new string[Length];
		this.count	    = 0;
                for (int i = 0; i < Length; i++)
                    this.PATH[i] = null;
            }
        }

        public struct TRANSACTION
        {
            public string   CODE;
            public string   TRANSACTION_FORM;

            public int      DATE;
            public int      TIME;

            public int      PRICE;
            public int      AMOUNT;
        }

    
}


