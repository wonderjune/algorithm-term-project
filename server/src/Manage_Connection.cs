﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;

namespace Alg_SERVER
{
    public class Manage_Connection
    {
        private IPEndPoint  SERVER_Address;
        private Socket      SERVER_Socket;

        private IPEndPoint  CLIENT_Address;
        private Socket      CLIENT_Socket;



        public Manage_Connection()
        { 
        }


        public void Open_Connection( int SERVER_PortNumber)
        {
            this.SERVER_Address     = new IPEndPoint    (IPAddress.Any, SERVER_PortNumber);
            this.SERVER_Socket      = new Socket        (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            
            this.SERVER_Socket.Bind(this.SERVER_Address);
            this.SERVER_Socket.Listen(20);


            Console.WriteLine("Waiting for CLIENT");
            this.CLIENT_Socket      = this.SERVER_Socket.Accept();
            this.CLIENT_Address     = (IPEndPoint)this.CLIENT_Socket.RemoteEndPoint;


            Console.WriteLine("CLIENT Connected");
        }

        public void Close_Connection()
        { 
        }



        public  IPEndPoint  Server_Address
        {
            get { return this.SERVER_Address; }
        }

        public  IPEndPoint  Client_Address
        {
            get { return this.CLIENT_Address; }
        }


        public  Socket      Server_Socket
        {
            get { return this.SERVER_Socket; }
        }

        public  Socket      Client_Socket
        {
            get { return this.CLIENT_Socket; }
        }

    }
}
