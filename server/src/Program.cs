﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.IO;

using System.Net;
using System.Net.Sockets;

namespace Alg_SERVER
{
    public class Program
    {
        static void Main(string[] args)
        {
            ////////////////////////////////////////////////////////////////////
            ////// Change the value if needed 
            int PortNumber              = 7000;
            int MaximumTickListLength   = 200000;
            int MAXLENGTH_OF_TRLIST     = 5000;
            ////////////////////////////////////////////////////////////////////

            #region [0] Read Training Data 

                Manage_FileInOut oMFIO = new Manage_FileInOut();
                oMFIO.ReadTrainingData_MakeDateList("./data/");
                oMFIO.get_LastIDX_DIList();

                Manage_TickData oMTD = new Manage_TickData(MaximumTickListLength);
                oMTD.getTickDATA_Make_TickList(oMFIO, oMFIO.NumberOfCompany_in_TrainingData);
                oMTD.Index_OneTick_afterSEND();            
            
            #endregion

            #region [1] Open Connection. Wait for client

                Manage_Connection oMC = new Manage_Connection();
                oMC.Open_Connection(PortNumber);

            #endregion

            #region [2] Receive, Send

                Manage_Transmission_Reception oMTR = new Manage_Transmission_Reception(oMC.Client_Socket, MAXLENGTH_OF_TRLIST);
                oMTR.ReceiveMsg(oMC.Client_Socket, oMTD, oMFIO, oMFIO.NumberOfCompany_in_TrainingData);
            
            #endregion

            #region [3] STORE TRANSACTION INFO

                STORE_Transaction_Information oSTRI = new STORE_Transaction_Information();
                oSTRI.STORE_TRANSACTIONLIST_LIST("./data", oMTR);

            #endregion

            #region [4] Wait 

                oMTR.Wait();

            #endregion


        }
    }
}
