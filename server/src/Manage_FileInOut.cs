﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.IO;

namespace Alg_SERVER
{
    public class Manage_FileInOut
    {

        private     int         NumberOf_Company_in_TrainingData;

        // []종목 * 날짜 arr 
        private     string[]    Company_List;
        private     int[,]      DateList;

        private     int[]       Merged_DateList;

        private     int[]       Sorted_DateList;

        private     IIDX[]      Date_IDXList;
        private     int         count_DIL;

	private int recentSortedListSize;

        public Manage_FileInOut ()
        {
        }


        public void ReadTrainingData_MakeDateList( string Alg_SERVER_DATA_Folder_Path)
        {

            // [1] Read code.list
            this.Company_List       = Read_Company_Table(Alg_SERVER_DATA_Folder_Path);

            // [2] set NumberOf_Company_inTrainingData
            this.NumberOf_Company_in_TrainingData = this.Company_List.Length;

            // [2] get DateList 
            this.DateList           = Read_ALL_TrainingData_Fillout_DateList(Alg_SERVER_DATA_Folder_Path, this.Company_List, this.Company_List.Length);

            // [3] Merge DateList
            this.Merged_DateList    = getMerged_List(this.DateList, this.Company_List.Length);

            // [4] get Sorted List 
	    Console.Write("Sorting DateList... ");

            this.Sorted_DateList    = Sort_List(this.Merged_DateList);

/*	    this.Sorted_DateList = (int [])this.Merged_DateList.Clone();

	    Array.Sort<int>(this.Sorted_DateList, delegate(int i1, int i2) {
			    return (i1 - i2 != 0) ? i1 - i2 : -1;
			    });*/

	    Console.WriteLine("done. ({0} items)", recentSortedListSize);

            // [5] get Date_IDXList
            this.Date_IDXList       = getIIDX(Alg_SERVER_DATA_Folder_Path, this.Sorted_DateList, this.Company_List, this.Company_List.Length);
              
        }

        public void get_LastIDX_DIList()
        {
            this.count_DIL = 0;
            for (int i = 0; i < this.Date_IDXList.Length; i++)            
            {
                if (Date_IDXList[i].DATE > 0)
                    this.count_DIL++;
            }
        }



        #region File Inout Method

            private int[,] Read_ALL_TrainingData_Fillout_DateList   (   string Alg_SERVER_DATA_Folder_Path, string[] Company_List, int NumberOf_Company)
            {
                
                string FullName = Alg_SERVER_DATA_Folder_Path + "raw/";
                string[] TickData_FileName;


                int[,] Date_List = new int[NumberOf_Company, Directory.GetFiles(FullName + Company_List[0]).Length + 50];

                

                
                for( int N = 0 ; N < NumberOf_Company ; N++ )
                {
                    // get all File of 'N'th company
                    Console.Write("Company {0} of {1} ", N + 1, NumberOf_Company);
                    TickData_FileName = Directory.GetFiles(FullName + Company_List[N]);
                    Console.WriteLine("({0} files)... ", TickData_FileName.Length);
                    for (int i = 0; i < TickData_FileName.Length; i++)
                    {
                        Date_List[N,i] = Read_Raw_Tabe_aLine(TickData_FileName[i]).DATE;
                    }

                }

                return Date_List;
                /**/

            }

            private Tick[] Read_Raw_Table                           (   string FileName)
            {
                // FilePath로는 여러 종목이 있는 큰 폴더 까지만 준다.
                int count = 0 ;
                int CODE_Length = 0;

                string a_Line = null;
                char[] Spliter = new char[1];
                string[] Raw_Features = new string[7];
                Tick[] RawDataBig = new Tick[ 50000 ];


          
                FileStream file = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
            
                
                // Separator 로 '/' 할당
                Spliter[0] = '/';
                
               
                while (sr.EndOfStream == false)
                {
                    // 한줄을 읽어서 a_Line 에 저장 
                    a_Line = sr.ReadLine();

                    // 이 한 줄을 Spliter 로 나누어 이를 String 배열 Raw_Featuers 에 저장
                    Raw_Features = a_Line.Split(Spliter, StringSplitOptions.RemoveEmptyEntries);

                    // 이를 Raw_Table 에 저장 => indexing 조심

                    // SHCode 는 약간 특별한 방식으로 저장해야 한다.
                    CODE_Length =  Raw_Features[0].Length ;
                    for (int i = 0; i < 6 - CODE_Length; i++)
                        Raw_Features[0] = "0" + Raw_Features[0];

                    RawDataBig[count].SH_CODE = Raw_Features[0];

                    RawDataBig[count].DATE = String_to_Int(Raw_Features[1]);
                    RawDataBig[count].TIME = String_to_Int(Raw_Features[2]);
                    RawDataBig[count].PRICE = String_to_Int(Raw_Features[3]);
                    RawDataBig[count].TICK_VOLUME = String_to_Int(Raw_Features[4]);
                    RawDataBig[count].INC_OR_DEC = String_to_Int(Raw_Features[5]);

                    count++;
                }

                sr.Close();
                file.Close();


                return RawDataBig ;
            }

            private Tick Read_Raw_Tabe_aLine                        (   string FileName)
            {

                // FilePath로는 여러 종목이 있는 큰 폴더 까지만 준다.
                int CODE_Length = 0;

                string a_Line = null;
                char[] Spliter = new char[1];
                string[] Raw_Features = new string[7];
                Tick RawDataBig = new Tick();




                FileStream file = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);


                // Separator 로 '/' 할당
                Spliter[0] = '/';


               
                // 한줄을 읽어서 a_Line 에 저장 
                a_Line = sr.ReadLine();

                // 이 한 줄을 Spliter 로 나누어 이를 String 배열 Raw_Featuers 에 저장
                Raw_Features = a_Line.Split(Spliter, StringSplitOptions.RemoveEmptyEntries);

                // 이를 Raw_Table 에 저장 => indexing 조심

                // SHCode 는 약간 특별한 방식으로 저장해야 한다.
                CODE_Length = Raw_Features[0].Length;
                for (int i = 0; i < 6 - CODE_Length; i++)
                    Raw_Features[0] = "0" + Raw_Features[0];

                RawDataBig.SH_CODE = Raw_Features[0];

                RawDataBig.DATE = String_to_Int(Raw_Features[1]);
                RawDataBig.TIME = String_to_Int(Raw_Features[2]);
                RawDataBig.PRICE = String_to_Int(Raw_Features[3]);
                RawDataBig.TICK_VOLUME = String_to_Int(Raw_Features[4]);
                RawDataBig.INC_OR_DEC = String_to_Int(Raw_Features[5]);

                

                sr.Close();
                file.Close();


                return RawDataBig;
            }

            private string[] Read_Company_Table                     (   string Alg_SERVER_DATA_Folder_Path)
            {
                char[] Spliter = new char[1];


                string FullName = Alg_SERVER_DATA_Folder_Path + "code.list" ;

                // return 할 배열 
                string[] tempReturnArray = new string[100];
                string[] ReturnArray;
                int CountRA = 0;


                // FileStream Open
                FileStream file = new FileStream(FullName, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);


                // 리턴 배열 초기화 
                for (int i = 0; i < 100; i++)                
                        tempReturnArray[i] = null;

                CountRA = 0;
                while (sr.EndOfStream == false)
                {
                    // 한줄을 리턴어레이 한칸에 저장한다
                    //종목번호
                    tempReturnArray[CountRA] = sr.ReadLine();
                    CountRA++;
                }

                sr.Close();
                file.Close();

                ReturnArray = new string[CountRA];
                for (int i = 0; i < CountRA; i++)
                    ReturnArray[i] = tempReturnArray[i];

                return ReturnArray;

            }

            private int String_to_Int                               (   string strings)
            {
                long returnvalue = 0;
                long CurValue = 0;
                int Length = 0;


                // 길이를 구한다
                Length = strings.Length;
                
                for (int i = 0; i <= Length - 1; i++)
                {
                    CurValue = strings[i] - '0';

                    for (int j = 0; j < (Length - 1 - i); j++)
                    {
                        CurValue = CurValue * 10;
                    }
                    returnvalue = returnvalue + CurValue;
                }               


                return (int)returnvalue;
            }

        #endregion


        #region Tools
            private bool compare(int p, int q)
            {
                if (p == 0 && q == 0) return true;
                if (p == 0) return false;
                if (q == 0) return true;
                return p < q;
            }
            private int find_binary(int []data,int key, int mode,int n)
            {
                int l, r;
                int mid;
                l = 0; r = n;
                while (l < r)
                {
                    mid = (l + r) / 2;
                    if (compare(data[mid] , key))
                    {
                        l = mid+1;
                    }
                    else if (compare(key,data[mid]))
                    {
                        r = mid;
                    }
                    else
                    {
                        if (mode == 0)
                        {
                            r = mid;
                        }
                        else
                        {
                            l = mid + 1;
                        }
                    }
                }
                return l;
            }
            private IIDX[] getIIDX                                  (   string Alg_SERVER_DATA_Folder_Path,  int [] SortedList, string [] CompList,  int NumberOf_Com)
            {
                // [0] Initialize
                IIDX[] DateIDXList = new IIDX[SortedList.Length];
                for (int i = 0; i < DateIDXList.Length; i++)
                    DateIDXList[i] = new IIDX(NumberOf_Com);
                string FullName = Alg_SERVER_DATA_Folder_Path + "raw/";
                string[] TickData_FileName;
                // [1] Date 를 채운다.
                for (int i = 0; ((i < SortedList.Length) && (SortedList[i] > 0)); i++)
                    DateIDXList[i].DATE = SortedList[i];
                // [2] 하나의 Date에 대해
                    //Company_List[NumberOf_Com ]
                for (int N = 0; N < NumberOf_Com; N++)
                {
                    TickData_FileName = Directory.GetFiles(FullName + Company_List[N]);
                    for (int f = 0; f < TickData_FileName.Length; f++)
                    {
                        int i=0;
                        int si = 0;
                        int ei = SortedList.Length;
                        int k = Read_Raw_Tabe_aLine(TickData_FileName[f]).DATE;
                        si = find_binary(SortedList, k, 0, SortedList.Length);
                        ei = find_binary(SortedList, k, 1, SortedList.Length);
                        for (i=si; ((i < ei) && (SortedList[i] > 0)); i++)
                        {
                            //Console.WriteLine("{0}   {1}  ", DateIDXList[i].DATE, Read_Raw_Tabe_aLine(TickData_FileName[f]).DATE);
                            if (DateIDXList[i].DATE == k)
                            {
//                                Console.WriteLine("{0}   {1}  ", DateIDXList[i].DATE, TickData_FileName[f]);
                                DateIDXList[i].PATH[DateIDXList[i].count] = TickData_FileName[f];
                                DateIDXList[i].count++;
                            }
                        }
                    }
                }
                return DateIDXList;
            }

            private int[] getMerged_List                            (   int [,] Whole_Date_List, int NumberOf_Com)
            {
                int[] Merged_List = new int[ Whole_Date_List.GetLength(1) + 100 ];
                int MLIDX = 0 ;


                // for All New item
                for( int row = 0 ; row < NumberOf_Com ; row++ )
                {
                    for (int Colume = 0; Colume < Whole_Date_List.GetLength(1); Colume++)
                    {
                        if (Compare_ONE_vs_List(Whole_Date_List[row,Colume], Merged_List, MLIDX) == true)
                        {
                            Merged_List[ MLIDX ] = Whole_Date_List[row,Colume] ;
                            MLIDX++; 
                        }
                    }
                }

                return Merged_List; 
            }

            private bool Compare_ONE_vs_List                        (   int OneNewData, int[] ExistingList, int ExListLength)
            {                

                for (int i = 0; i < ExListLength; i++)
                {
                    if (OneNewData == ExistingList[i])
                        return false;

                    if (OneNewData == 0)
                        return false;

                    //if (ExistingList[i] == 0)
                      //  return false; 
                }

                return true; 
            }

            private int[] Sort_List                                 (   int[] ExistingList)
            {
                int[] ExLists = ExistingList ;
                int[] Sorted_List = new int[ExistingList.Length];

                int countSL = 0 ;
                int curMinIDX  = 0 ;

		recentSortedListSize = 0;
		for (int i = 0; i < ExLists.Length; i++)
			if (ExLists[i] > 0) recentSortedListSize++;
                
                while ( curMinIDX > -1)
                {
                    curMinIDX = Find_MinIDX(ExLists);

                    if (curMinIDX > -1)
                    {
                        Sorted_List[countSL] = ExLists[curMinIDX];
                        countSL++;

                        ExLists[curMinIDX] = 0;
                    } 
                }
                /**/
                return Sorted_List;
            }

            private int Find_MinIDX                                 (   int[] List  )
            {
                // Find Minimum, but not 0 
                int MinValue = 99999999 ;
                int MinIDX = -1;
                for ( int i = 0; i < List.Length; i++ )
                {
                    if ( (List[i] > 0) && (List[i] < MinValue) )
                    {
                        MinValue = List[i];
                        MinIDX = i;
                    }
                }

                return MinIDX; 
            }

        #endregion





        public IIDX[] DateIDXList
        {
            get { return this.Date_IDXList; }
        }

        public string[] CompanyList
        {
            get { return this.Company_List; }
        }

        public int lastDAY
        {
            get { return this.count_DIL; }
        }

        public int NumberOfCompany_in_TrainingData
        {
            get { return this.NumberOf_Company_in_TrainingData; }
        }
    }
}
