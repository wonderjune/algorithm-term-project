﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;



namespace Alg_SERVER
{
    public class Manage_TickData
    {
        private string CUR_TICK;
        private Tick[] TickList;
        private Tick[] Sorted_TickList;

        private bool ENDOF_TICKDATA;
        private bool ENDOF_ONEDAY;

        int curIDX_TL;
        int LastIDX_TL;
        int curIDX_Day;

	private int recentSortedListSize;

        public                  Manage_TickData                 (   int TickListLength)
        {
            this.TickList           = new Tick[TickListLength];
            this.Sorted_TickList    = new Tick[TickListLength];
            this.CUR_TICK           = null;

            this.ENDOF_TICKDATA = false;
            this.ENDOF_ONEDAY = false;


            this.LastIDX_TL = 0;    //  Tick List
            this.curIDX_TL = 0;


            this.curIDX_Day = 0;
        }

        public void             getTickDATA_Make_TickList       (   Manage_FileInOut oMFIO, int Number_Of_Company)
        {
            // [4] Status Update
            if (this.curIDX_Day == oMFIO.lastDAY)
                this.ENDOF_TICKDATA = true;

            if (this.ENDOF_TICKDATA == false)
            {
                // [1] for All Company
                for (int i = 0; ((i < Number_Of_Company) && (oMFIO.DateIDXList[this.curIDX_Day].PATH[i] != null)); i++)
                    this.TickList = Add_NewList_setLastIDX_TL(this.TickList, this.LastIDX_TL, Read_Raw_Table(oMFIO.DateIDXList[this.curIDX_Day].PATH[i]));

                // [2] sort List
	        Console.Write("Sorting TickList... ");

//		for(int i=0; i<20; i++)
//			Console.WriteLine("{0}<",TickList[i].TIME);
                this.Sorted_TickList = Sort_List(this.TickList);

/*		this.Sorted_TickList = (Tick []) this.TickList.Clone();

		Array.Sort<Tick>(this.Sorted_TickList, delegate(Tick t1, Tick t2) {
				return (t1.TIME - t2.TIME != 0) ? t1.TIME - t2.TIME : -1;
				});*/

		Console.WriteLine("done. ({0} items)", recentSortedListSize);

		// [3] Update
                this.curIDX_Day++;
            }            
        }


        public void             Index_OneTick_afterSEND         (   )
        {
            this.CUR_TICK =     this.Sorted_TickList[this.curIDX_TL].SH_CODE                    + '/' + 
                                this.Sorted_TickList[this.curIDX_TL].DATE.ToString()            + '/' +
                                this.Sorted_TickList[this.curIDX_TL].TIME.ToString()            + '/' +
                                this.Sorted_TickList[this.curIDX_TL].PRICE.ToString()           + '/' +
                                this.Sorted_TickList[this.curIDX_TL].TICK_VOLUME.ToString()     + '/' +
                                this.Sorted_TickList[this.curIDX_TL].INC_OR_DEC.ToString()      + '/' ;
            this.curIDX_TL++;


            // [4] Status Update
            if (this.curIDX_TL == this.LastIDX_TL)
                this.ENDOF_ONEDAY = true;

        }

        public void             Initialize                      (   )
        {
            for (int i = 0; i < this.TickList.Length; i++)
            {
                this.TickList[i] = new Tick();
                this.Sorted_TickList[i] = new Tick();
            }

            this.LastIDX_TL = 0;
            this.curIDX_TL = 0;
            this.ENDOF_ONEDAY = false;
        }



        #region Tools
		private Tick daeip(Tick t)
		{
			Tick p;
			p.TIME = t.TIME;
			p.SH_CODE = t.SH_CODE;
			p.DATE = t.DATE;
			p.PRICE = t.PRICE;
			p.TICK_VOLUME = t.TICK_VOLUME;
			p.INC_OR_DEC = t.INC_OR_DEC;
			return p;
		}
		private bool compare(int p,int q)
		{
			if(p==0 && q==0)return true;
			if (p == 0) return false;
			if (q == 0) return true;
			return p <= q;
		}
		private void q_sort(Tick[] data,int l,int r)
		{
			if(r-l <= 1)return;
			Tick key = data[l];
			Tick temp;
			int ll,rr;
			ll=l+1;rr=r-1;
			while(ll < rr)
			{
				while(ll < r-1 && compare(data[ll].TIME , key.TIME)) ll++;
				while(rr > l && compare(key.TIME, data[rr].TIME)) rr--;
				if(ll >= rr)break;
				temp = data[ll];
				data[ll] = data[rr];
				data[rr] = temp;
			}
			temp = data[rr];
			data[rr] = data[l];
			data[l] = temp;
			q_sort(data, l, rr);
			q_sort(data, ll, r);
		}
            private Tick[]      Sort_List                       (   Tick[] ExistingList)
	    {
		recentSortedListSize = 0;
		for (int i = 0; i < ExistingList.Length; i++)
			if (ExistingList[i].TIME > 0 && ExistingList[i].TIME < 99999999) recentSortedListSize++;
	        Tick[] NewList = new Tick[ExistingList.Length];
		for (int i = 0; i < NewList.Length; i++) {
//			if (ExistingList[i].TIME > 0 && ExistingList[i].TIME > 99999999)
				NewList[i] = ExistingList[i];
		}
	        q_sort(NewList, 0, NewList.Length);
//		for(int i=0; i<20; i++)
//			Console.WriteLine("{0}>",NewList[i].TIME);
		return NewList;
		   /*
                Tick[] ExLists = ExistingList;
                Tick[] Sorted_List = new Tick[ExistingList.Length];

                int countSL = 0 ;
                int curMinIDX  = 0 ;
                
                while ( curMinIDX > -1)
                {
                    curMinIDX = Find_earlestIDX(ExLists);

                    if (curMinIDX > -1)
                    {
                        Sorted_List[countSL] = ExLists[curMinIDX];
                        countSL++;

                        ExLists[curMinIDX].TIME = 0;
                    } 
                }
                return Sorted_List;
*/
            }

            private int         Find_earlestIDX                 (   Tick[] List)
            {
                // Find Minimum, but not 0 
                int MinValue = 99999999;
                int MinIDX = -1;

                for (int i = 0; i < List.Length; i++)
                {
                    if ((List[i].TIME > 0) && (List[i].TIME < MinValue))
                    {
                        MinValue = List[i].TIME;
                        MinIDX = i;
                    }
                }

                return MinIDX;
            }


            private Tick[]      Add_NewList_setLastIDX_TL       (   Tick[] ExistingList, int idxEL, Tick[] NewList)
            { 
                 Tick[] updatedExList =  ExistingList ;
                 for (int i = 0; ((i < 50000) && (NewList[i].DATE > 0)); i++)
                 {
                     updatedExList[idxEL + i] = NewList[i];
                     this.LastIDX_TL++;
                 }

                 return updatedExList;

            }

            private Tick[]      Read_Raw_Table                  (   string FileName)
            {
                // FilePath로는 여러 종목이 있는 큰 폴더 까지만 준다.
                int count = 0 ;
                int CODE_Length = 0;

                string a_Line = null;
                char[] Spliter = new char[1];
                string[] Raw_Features = new string[7];
                Tick[] RawDataBig = new Tick[ 50000 ];


          
                FileStream file = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
            
                
                // Separator 로 '/' 할당
                Spliter[0] = '/';
                
               
                while (sr.EndOfStream == false)
                {
                    // 한줄을 읽어서 a_Line 에 저장 
                    a_Line = sr.ReadLine();

                    // 이 한 줄을 Spliter 로 나누어 이를 String 배열 Raw_Featuers 에 저장
                    Raw_Features = a_Line.Split(Spliter, StringSplitOptions.RemoveEmptyEntries);

                    // 이를 Raw_Table 에 저장 => indexing 조심

                    // SHCode 는 약간 특별한 방식으로 저장해야 한다.
                    CODE_Length =  Raw_Features[0].Length ;
                    for (int i = 0; i < 6 - CODE_Length; i++)
                        Raw_Features[0] = "0" + Raw_Features[0];

                    RawDataBig[count].SH_CODE = Raw_Features[0];

                    RawDataBig[count].DATE = String_to_Int(Raw_Features[1]);
                    RawDataBig[count].TIME = String_to_Int(Raw_Features[2]);
                    RawDataBig[count].PRICE = String_to_Int(Raw_Features[3]);
                    RawDataBig[count].TICK_VOLUME = String_to_Int(Raw_Features[4]);
                    RawDataBig[count].INC_OR_DEC = String_to_Int(Raw_Features[5]);

                    count++;
                }                

                sr.Close();
                file.Close();


                return RawDataBig ;
            }

            private static int  String_to_Int                   (   string strings)
            {
                long returnvalue = 0;
                long CurValue = 0;
                int Length = 0;


                // 길이를 구한다
                Length = strings.Length;

                for (int i = 0; i <= Length - 1; i++)
                {
                    CurValue = strings[i] - '0';

                    for (int j = 0; j < (Length - 1 - i); j++)
                    {
                        CurValue = CurValue * 10;
                    }
                    returnvalue = returnvalue + CurValue;
                }


                return (int)returnvalue;
            }

        #endregion



        public string CURTICK
        {
            get { return this.CUR_TICK; }
        }

        public bool ENDOF_TICK_DATA
        {
            get { return this.ENDOF_TICKDATA; }
        }

        public bool ENDOF_ONE_DAY
        {
            get { return this.ENDOF_ONEDAY; }
        }


        public Tick[] SortedTickList
        {
            get { return this.Sorted_TickList; }
        }

        public int curIDXTL
        {
            get { return this.curIDX_TL; }
        }
    }
}
