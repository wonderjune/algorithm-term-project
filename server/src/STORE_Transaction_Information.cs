﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace Alg_SERVER
{
    public class STORE_Transaction_Information
    {
        public STORE_Transaction_Information()
        { }


        public void STORE_TRANSACTIONLIST_LIST(string PATH, Manage_Transmission_Reception oMTR)
        {
            char Spliter = '/';
            string FullName = PATH; 
            FullName = FullName + "tx.log";


            // Open File Stream
            FileStream file = new FileStream(FullName, FileMode.CreateNew, FileAccess.Write);
            StreamWriter sw = new StreamWriter(file);            

            if (oMTR.lastTRL == 0)
                sw.WriteLine("No Transaction ");

            for (int i = 0; i < oMTR.lastTRL; i++)
            {
                // CODE
                sw.Write(oMTR.TRANSACTIONLIST[i].CODE );
                sw.Write(Convert.ToString(Spliter));

                // TRANSACTION_FORM
                sw.Write(oMTR.TRANSACTIONLIST[i].TRANSACTION_FORM);
                sw.Write(Convert.ToString(Spliter));

                // DATE
                sw.Write(oMTR.TRANSACTIONLIST[i].DATE.ToString());
                sw.Write(Convert.ToString(Spliter));

                // TIME
                sw.Write(oMTR.TRANSACTIONLIST[i].TIME.ToString());
                sw.Write(Convert.ToString(Spliter));

                // PRICE
                sw.Write(oMTR.TRANSACTIONLIST[i].PRICE.ToString());
                sw.Write(Convert.ToString(Spliter));

                // AMOUNT
                sw.Write(oMTR.TRANSACTIONLIST[i].AMOUNT.ToString());
                sw.Write(Convert.ToString(Spliter));

                sw.WriteLine();
            }

            /**/
            sw.Close();
            file.Close();
        }


    }
}
